import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from '../views/App'
import Dashbord from '../views/Dashbord'
import Users from '../views/Users'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'dashbord',
            component: Dashbord
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
